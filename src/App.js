import React from "react";
import { hot } from 'react-hot-loader/root';
import NumberPad from "./Components/NumberPad";
import '../css/NumberPadButton.css'
import Theme from './Components/Theme'


class App extends React.Component {   
   constructor(props) {
      super(props)
   
      this.state = {
          
      }
      this.backgroundBlack = false;
      this.isScienticMode = false;
   }

   themeChange = (e) => {
      if(e.target.innerText == "Dark Theme"){
         
         this.backgroundBlack = true;
      }
      if(e.target.innerText == "Light Theme"){
         this.backgroundBlack = false
      }
      if(e.target.innerText == "Scientific Mode"){
         this.isScienticMode = !this.isScienticMode;
         console.log("scientific mode changed",this.isScienticMode);
      }

      this.forceUpdate()
   }
    
   render() {   
      let changeBackground = ""
      if(this.backgroundBlack){
         changeBackground = "bg-black"
      }

      return (
            <div className={changeBackground} >
               <Theme changeTheme={this.themeChange} themChangeActive={this.backgroundBlack} />
               <NumberPad themChangeActive={this.backgroundBlack} scientificMode={this.isScienticMode} />
            </div>
      );
   }
}
export default hot(App);