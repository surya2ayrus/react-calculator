import React, { Component } from 'react'
import NumberPadButton from './NumberPadButton'
import '../../css/NumberPadButton.css'
import Theme from './Theme'


class NumberPad extends Component {
    constructor(props) {
        super(props)

        this.state = {
            calValue: "",
            prevInput: "",
            operator: "",
            prevOperator: "",
            isOperatorAdded: false,
        }
        // this.array = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        this.storeSignArr = [];
        this.initialState = this.state;
    }

    setOperator(operatorArg){
        let result,prevOpr;
        if(this.state.operator){
            prevOpr = this.state.operator;
            result = this.resultByOperator(prevOpr);
            this.setState({
                prevOperator: this.state.operator,
                operator: operatorArg,
                isOperatorAdded: true,
                prevInput: result,
                calValue: result,
            },() => {
                console.log("inside call back",this.state);
            })
        } else {
            this.setState({
                operator: operatorArg,
                isOperatorAdded: true,
                prevInput: this.state.calValue
            })
        }
    }

    resultByOperator(Opr){
        switch (Opr){
            case "+":
                return parseInt(this.state.prevInput) + parseInt(this.state.calValue);
            case "-":
                return this.state.prevInput - this.state.calValue;
            case "*":
                return this.state.prevInput * this.state.calValue;
            case "/":
                return this.state.prevInput / this.state.calValue;
            case "**":
                return this.state.prevInput * this.state.prevInput;
            case "sqrt":
                return Math.sqrt(this.state.prevInput);
            case "=":
                return this.state.prevInput;
        }
    }

    numberClicked = (e) => {
        console.log("clicked",e.target.innerText);
        let clickedText = e.target.innerText;
        
        switch(clickedText){
            case "Add (+)":
                console.log("inside add");
                if(!this.state.isOperatorAdded){
                    this.setOperator("+")
                }
                break;
            case "Subract (-)":
                console.log("inside sub");    
                if(!this.state.isOperatorAdded){
                    this.setOperator("-")
                }
                break;
            case "Multiply (*)":
                console.log("inside multiply");
                if(!this.state.isOperatorAdded){
                    this.setOperator("*")
                }
                break;
            case "Divide (/)":
                console.log("inside divide");
                if(!this.state.isOperatorAdded){
                    this.setOperator("/")
                }
                break;
            case "Sqr (**)":
                console.log("inside square");
                if(!this.state.isOperatorAdded){
                    this.setOperator("**");
                }
                break;
            case "Sqrt":
                if(!this.state.isOperatorAdded){
                    this.setOperator("sqrt");
                }
                break;
            case "=":
                console.log("inside =");
                // if(!this.state.isOperatorAdded){
                    this.setOperator("=")
                // }
                break;
            case "Clear":
                console.log("inside clear");
                this.setState(this.initialState,()=> {
                    console.log("state value after clearing",this.state);
                });
                break;
            default:
                
                if(this.state.isOperatorAdded){
                    this.setState({
                        calValue: e.target.innerText,
                        isOperatorAdded: false
                    },() => {
                        console.log("inside default",this.state);
                    })
                } else {
                    this.setState({
                        calValue: this.state.calValue + e.target.innerText
                    },()=>{
                        console.log("value of input",this.state);
                    })
                }
                break;
        }

    }


    render() {
        // console.log("theme change in number pad",this.props.themChangeActive)
        let result;
        let changeInputTheme = "row display-measurement";
        let addOrSqaure = "Add (+)";
        let subOrSqrt = "Subract (-)"
        if(this.state.calValue.length || this.state.calValue){
            result = this.state.calValue
        }
        if(this.props.themChangeActive){
            changeInputTheme = "row display-measurement-black"
        }
        if(this.props.scientificMode){
            addOrSqaure = "Sqr (**)";
            subOrSqrt = "Sqrt";
        }

        return (
            <div className="container col-6 my-auto numpad-bg">
                {/* {
                    this.array.map((number,index) => {
                        return <NumberPadButton key={number} number={number} />
                    })
                }         */}
                
                <div className={changeInputTheme}>{result}</div>
                <div className="row">
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"1"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"2"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"3"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={addOrSqaure}/>
                </div>

                <div className="row">
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"4"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"5"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"6"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={subOrSqrt}/>
                </div>

                <div className="row">
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"7"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"8"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"9"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"Multiply (*)"}/>
                </div>

                <div className="row">
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"Clear"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"0"}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"="}/>
                    <NumberPadButton themChangeActive={this.props.themChangeActive} numberClick={this.numberClicked} text={"Divide (/)"}/>
                </div>

            </div>
        )
    }
}

export default NumberPad
