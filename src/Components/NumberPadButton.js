import React, { Component } from 'react'

class NumberPadButton extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    render() {
        
        let blackBackground = "col center-text numpad-button-border"
        console.log("props:",this.props.themChangeActive)
        if(this.props.themChangeActive){
            blackBackground = "col center-text numpad-button-border-active"
        }

        return (
            <div className={blackBackground} onClick={this.props.numberClick} >
                {this.props.text}
            </div>
        )
    }
}

export default NumberPadButton
