import React, { Component } from 'react'
import '../../css/Theme.css'

class Theme extends Component {
    render() {
        let backgroundBlack;
        if(this.props.themChangeActive){
            // backgroundBlack = 
        }

        return (
            <div className="container col btn-pos">
                <button className="btn btn-outline-primary custom-btn" onClick={this.props.changeTheme}>Light Theme</button>
                <button className="btn btn-outline-primary custom-btn" onClick={this.props.changeTheme}>Dark Theme</button> 
                <button className="btn btn-outline-primary custom-btn" onClick={this.props.changeTheme}>Scientific Mode</button>           
            </div>
        )
    }
}

export default Theme
